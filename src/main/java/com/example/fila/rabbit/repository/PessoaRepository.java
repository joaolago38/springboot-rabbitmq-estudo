package com.example.fila.rabbit.repository;

import com.example.fila.rabbit.model.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {
    List<Pessoa> findAlunoByNomeContainingIgnoreCase(String nome);
}
